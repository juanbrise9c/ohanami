import 'package:flutter/foundation.dart';

class Environment {
   static String apiUrl = defaultTargetPlatform == TargetPlatform.android ? 'http://10.0.2.2:3000/api/' : 'http://localhost:3000/api/';

}
