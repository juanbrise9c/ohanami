// ignore_for_file: avoid_print, unused_field

import 'dart:convert';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:ohanami/blocs/games/games_events.dart';
import 'package:http/http.dart' as http;
import 'package:ohanami/shared/enviroment.dart';


class GamesBloc extends Bloc<GamesEventos, List> {
  GamesBloc() : super([]) {
    on<GamesEventos>((event, emit) {
      if (event is GetGames) getData();
    });
  }

  // List _parties;

  Future<bool> getData() async {
    try {
      final uri = Uri.parse('${Environment.apiUrl}Games?access_token=y0y6uaCdsqKTwLZXDf33pQKZkR58JfWuglnLRVqEaq3JVLYL8yxmB2h1p4j7Szwu');
      final response = await http.get(uri);
      final data = json.decode(response.body);
      // _parties = data;
      return data;
    } catch (e) {
      print(e);
      throw Exception(e);
    }
  }
}
