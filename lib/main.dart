// ignore_for_file: prefer_const_constructors, avoid_unnecessary_containers, use_key_in_widget_constructors, prefer_final_fields, must_call_super

import 'package:flutter/material.dart';
import 'package:ohanami/blocs/games/bloc_games.dart';
import 'package:ohanami/blocs/games/games_events.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  GamesBloc _games = GamesBloc();
  @override
  void initState() {
    super.initState();
    _games.add(GetGames());
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Material App',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Partidas'),
        ),
        body: Center(
          child: ListView.builder(
            itemCount: 4,
            itemBuilder: (context, index) {
              return ListTile(
                title: Text('Jugador: Juan'),
                subtitle: Text('Puntuaje: 110'),
                leading: CircleAvatar(
                  child: Text((index + 1).toString()),
                ),
                trailing: Icon(Icons.arrow_forward_ios),
                onLongPress: () {
                  _deleteGame(context, index);
                },
              );
            }
          )
        ),
      ),
    );
  }

  _deleteGame(context, persona) {
    showDialog(
      context: context, 
      builder: ( _ ) => AlertDialog(
        title: Text('Eliminar Partida'),
        content: Text('¿Estas seguro de eliminar esta partida?'),
        actions: [],
      )
    );
  }
}
